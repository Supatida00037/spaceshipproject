﻿using System;
using Spaceship;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace Manager
{
    public class GameManager : MonoSingleton<GameManager>
    {
        [SerializeField] private Button startButton;
        [SerializeField] private RectTransform dialog;
        [SerializeField] private PlayerSpaceship playerSpaceship;
        [SerializeField] private EnemySpaceship enemySpaceship;
        
        [SerializeField] private EnemySpaceship enemySpaceship1;
        [SerializeField] private EnemySpaceship enemySpaceship2;
        
        [SerializeField] private ScoreManager scoreManager;
        public event Action OnRestarted;
        [SerializeField] private int playerSpaceshipHp;
        [SerializeField] private int playerSpaceshipMoveSpeed;
        [SerializeField] private int enemySpaceshipHp;
        
        [SerializeField] private int enemySpaceshipHp1;
        [SerializeField] private int enemySpaceshipHp2;
        
        [SerializeField] private int enemySpaceshipMoveSpeed;
        private int enemyScore;
        private int enemyCount;
        
        private void Awake()
        {
            Debug.Assert(startButton != null, "startButton cannot be null");
            Debug.Assert(dialog != null, "dialog cannot be null");
            Debug.Assert(playerSpaceship != null, "playerSpaceship cannot be null");
            Debug.Assert(enemySpaceship != null, "enemySpaceship cannot be null");
            
            Debug.Assert(enemySpaceship1 != null, "enemySpaceship cannot be null");
            Debug.Assert(enemySpaceship2 != null, "enemySpaceship cannot be null");
            
            Debug.Assert(scoreManager != null, "scoreManager cannot be null");
            Debug.Assert(playerSpaceshipHp > 0, "playerSpaceship hp has to be more than zero");
            Debug.Assert(playerSpaceshipMoveSpeed > 0, "playerSpaceshipMoveSpeed has to be more than zero");
            Debug.Assert(enemySpaceshipHp > 0, "enemySpaceshipHp has to be more than zero");
            
            Debug.Assert(enemySpaceshipHp1 > 0, "enemySpaceshipHp has to be more than zero");
            Debug.Assert(enemySpaceshipHp2 > 0, "enemySpaceshipHp has to be more than zero");
            
            Debug.Assert(enemySpaceshipMoveSpeed > 0, "enemySpaceshipMoveSpeed has to be more than zero");
            
            startButton.onClick.AddListener(OnStartButtonClicked);
            
        }

        private void OnStartButtonClicked()
        {
            dialog.gameObject.SetActive(false);
            StartGame();
        }

        private void StartGame()
        {
            scoreManager.Init(this);
            SpawnPlayerSpaceship();
            SpawnEnemySpaceship();
        }
        
        private void SpawnPlayerSpaceship()
        {
            var spaceship = Instantiate(playerSpaceship);
            spaceship.Init(playerSpaceshipHp, playerSpaceshipMoveSpeed);
            spaceship.OnExploded += OnPlayerSpaceshipExploded;
        }

        private void OnPlayerSpaceshipExploded()
        {
            Restart();
        }

        private void SpawnEnemySpaceship()
        {
            enemyCount = 0;
            enemyScore = 0;
            var spaceship = Instantiate(enemySpaceship);
            spaceship.Init(enemySpaceshipHp, enemySpaceshipMoveSpeed);
            spaceship.OnExploded += OnEnemySpaceshipExploded;
            
            var spaceship1 = Instantiate(enemySpaceship1);
            spaceship1.Init(enemySpaceshipHp1, enemySpaceshipMoveSpeed);
            spaceship1.OnExploded += OnEnemySpaceshipExploded;
            
            var spaceship2 = Instantiate(enemySpaceship2);
            spaceship2.Init(enemySpaceshipHp2, enemySpaceshipMoveSpeed);
            spaceship2.OnExploded += OnEnemySpaceshipExploded;
        }

        private void OnEnemySpaceshipExploded()
        {
            enemyScore++;
            enemyCount++;
            scoreManager.SetScore(enemyScore);
            CountEnemy(enemyCount);
            //Restart();
        }

        private void CountEnemy(int count)
        {
            if (count == 3)
            {
                SceneManager.LoadScene("GameLevel2",LoadSceneMode.Single);
                //Restart();
            }
        }

        private void Restart()
        {
            DestroyRemainingShip();
            dialog.gameObject.SetActive(true);
            OnRestarted?.Invoke();
        }

        private void DestroyRemainingShip()
        {
            var remainingEnemy = GameObject.FindGameObjectsWithTag("Enemy");
            foreach (var enemy in remainingEnemy)
            {
                Destroy(enemy);
            }
            var remainingPlayer = GameObject.FindGameObjectsWithTag("Player");
            foreach (var player in remainingPlayer)
            {
                Destroy(player);
            }
        }

    }
}
