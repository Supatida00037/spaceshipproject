﻿using TMPro;
using UnityEngine;

namespace Manager
{
    public class ScoreManager : MonoSingleton<ScoreManager>
    {
        [SerializeField] private TextMeshProUGUI scoreText;
        [SerializeField] private TextMeshProUGUI FinalscoreText;

        private GameManager gameManager;
        private int playerScore;
        private GameMenagerLevel2 gameManagerLevel2;
        public void Init(GameManager gameManager)
        {
            this.gameManager = gameManager;
            this.gameManager.OnRestarted += OnRestarted;
            HideScore(false);
            SetScore(0);
        }
        public void Init(GameMenagerLevel2 gameMenagerLevel2)
        {
            this.gameManagerLevel2 = gameMenagerLevel2;
            this.gameManagerLevel2.OnRestarted += OnRestarted;
            HideScore(false);
            SetScore(0);
        }

        public void SetScore(int score)
        {
            scoreText.text = $"Score : {score}";
            playerScore = score;
        }
        
        private void Awake()
        {
            Debug.Assert(scoreText != null, "scoreText cannot null");
        }
        
        private void OnRestarted()
        {
            FinalscoreText.text = $"Player Score : {playerScore}";
            gameManagerLevel2.OnRestarted -= OnRestarted;
            HideScore(true);
            SetScore(0);
        }

        private void HideScore(bool hide)
        {
            scoreText.gameObject.SetActive(!hide);
        }
    }
}


