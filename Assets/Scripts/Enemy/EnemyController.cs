﻿using System.Collections;
using System.Collections.Generic;
using Spaceship;
using UnityEngine;
using UnityEngine.Serialization;

namespace Enemy
{
    public class EnemyController : MonoBehaviour
    {
        [SerializeField] private EnemySpaceship enemySpaceship;
        [SerializeField] private float chasingThresholdDistance;
        private bool moveRight;
        private Vector2 movementInput = Vector2.zero;
        private void Update()
        {
            MoveToPlayer();
            enemySpaceship.Fire();
        }

         private void MoveToPlayer()
         {
             var inputVelocity = movementInput * enemySpaceship.Speed;
             if (transform.position.x > 4f)//ขยับขึ้นลงแนวตั้ง
                 moveRight = false;
             if (transform.position.x < -5f)
                 moveRight = true;
             if (moveRight)
                 transform.position = new Vector2(transform.position.x + enemySpaceship.Speed * Time.deltaTime, transform.position.y);
             else
                 transform.position = new Vector2(transform.position.x - enemySpaceship.Speed * Time.deltaTime, transform.position.y);
         }
    }    
}

